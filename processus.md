# Processus
## Départ

Le point de départ est un programme développé par un des membres de Transquinquennal, utilisant tkinter pour afficher les compteurs de paroles de chaque participant·es.
Comme je m'y connais mieux en langages web qu'en applications desktop (mais que j'aime quand même biem Python), je me suis dit que je pouvais utiliser Flask pour construire la partie serveur de l'interface. Ça me permettrait d'intégrer facilement le code initial en Python dans la structure de l'application.

L'intérêt d'avoir une interface web est qu'en converissant le Raspberry Pi en point d'accès WiFi diffusant son propre réseau local, plusieurs appareils peuvent voir le graphique en temps réel du temps de parole de chacun.e. Pratique en réunion de travail si on a pas d'écran ou de projecteur à disposition, ou qu'on ne veut pas avoir à tourner la tête régulièrement si on n'est pas face à l'écran / la projection.

C'est aussi un moyen d'envisager une potentielle extension à d'autres usages que celui de la pièce de théâtre, puisqu'on peut imaginer que chaque participant·e se connecte à ce serveur local (qui peut tourner sur un Raspberry Pi ou un autre ordinateur d'ailleurs) pour à la fois voir le graphique mais aussi indiquer qu'iel parle. Cela enlèverait le besoin de boutons physiques et serait donc plus modulaire.

Le dernier avantage de ne pas créer une application "desktop" (je crois) c'est qu'on peut faire tourner l'application sur un Raspberry PI headless, c'est-à-dire sans écran ni souris. Puisque le Raspberry Pi 3 n'a pas beaucoup de RAM, alléger au maximum le besoin en ressources est nécessaire (l'affichage d'un environnement de bureau et d'une page web demande quand même de la ressource). Ça évitera les ralentissements sur un écran branché directement au RPi et permettra de l'afficher sur des écrans de meilleure qualité.


## État des lieux : qu'est-ce qu'on peut utiliser ?
### Hardware

+ Un Raspberry Pi modèle 3 au moins, car il a une carte WiFi intégrée.
+ des boutons (???)
+ des câbles  (???)

Un des enjeux est de pouvoir utiliser l'application sur scène, et donc d'avoir des câbles suffisamment longs pour se balader sur une scène de théâtre. Le plus évident est d'utiliser des boutons branchés un USB, mais il faut donc un hub puisqu'il n'y a que 4 ports USB sur un RPi 3/4. Cependant il faut des rallonges USB, et est-ce que ces rallonges sont assez solides pour être baladées un peu partout ? Apparemment il existe de câbles audio très longs et résistants (câbles Canon, XLR ?), mais alors il faut des adaptateurs XLR > USB.

Une autre solution serait de brancher / souder les boutons directement sur les pins GPIO du RPi mais ça demande des compétences en éléctronique qui me dépassent pour l'instant (et la question des rallonges demeure toujours).

Ceci dit il y a pleins de modules Python pour interagir avec le RPi, donc ça devrait bien s'intégrer avec Flask. Il y a [PyUSB]() pour gérer les connexions USB et [Rpi.GPIO]() pour gérer les inputs/outputs des pins, donc les 2 options sont valables.

Apparemment il existe des panels/breakout boxes avec suffisamment de prises pour recevoir les 7 câbles XLR reliés aux boutons. Ces panels/breakout utilisent des prises DB-25 (un peu comme des VGA avec plus de pins) pour se brancher sur une interface. On peut théoriquement les [relier au RPi sur les GPIOS](https://medium.com/enekochan/raspberry-pi-gpio-port-to-idc26-db-25-ribbon-cable-b002321907fb), mais il y a aussi des [adaptateurs DB-25 → USB](https://www.amazon.com/db25-usb-adapter-Electronics/s?k=db25+to+usb+adapter&rh=n%3A172282) qui m'ont l'air tout à fait plus pratique.

### Software

Bon une fois que la question matérielle est réglée il reste à savoir comment on transfère ces inputs en graphique sur une page web.

Avec Flask il y a différents [tutos]() qui montre comment récupérer des infos venant des pins du RPi pour les afficher. On peut même contrôler les GPIOS depuis l'interface web ! C'est cool mais ça demande de rafraîchir la page périodiquement pour vérifier quels boutons ont été appuyés. Pour éviter ça je pourrais utiliser des requêtes AJAX à intervalle régulier pour vérifier s'il y a du nouveau côté serveur (et donc côté boutons appuyés) -- c'est ce qu'on appelle le [polling]().

Ça fonctionnerait pas trop mal je pense, mais
1. C'est pas très classe
2. C'est pas ultra précis (on n'a pas besoin d'une précision à la milliseconde mais si en live quelqu'un·e appuie sur un bouton et que l'écran avec le graphique met 1s à réagir ça fait pas très pro)
3. C'est pas hyper performant (surtout si on fait un polling très régulier pour avoir une précision convenable).

Alors je suis tombé sur une autre solution : les WEBSOCKETS (wouh).

C'est un moyen de créer une connexion FULL-DUPLEX entre le client et le serveur, c'est-à-dire que les 2 peuvent communiquer dans les 2 sens sans limite. C'est un peu comme un appel téléphonique, ou les 2 parties peuvent parler en même temps l'une à l'autre. Au contraire, la communication classique client-serveur est plutôt comme un échange de lettres. Le client envoie une demande au serveur, qui lui répond ensuite. C'est pratique mais ça a certaines limites. Un des désavantages des websockets (en tout cas dans un premier temps) c'est que ça demande d'avoir un serveur asynchrone avec plusieurs threads pour avoir beaucoup de clients en parallèles (puisque la connexion client-serveur reste active tous le temps pour permettre les échanges). Ça nous concerne pas trop puisque à court terme l'application sera utilisée pour être affichée sur un seul écran (un seul client). Si elle est utilisée en réunion il y aura peut-être un 20aine de clients au maximum en parallèle, ce qui reste raisonnable.

Bref, les websockets c'est la solution j'ai l'impression.


## Les mains dans le cambouis finalement

Après n'avoir pas réussi à implémenter les websockets depuis Flask (avec le module flask-socketio), je suis tombé sur [cette vidéo](https://piped.kavin.rocks/watch?v=tHQvTOcx_Ys) de Miguel Grinberg (qui a écrit plein de modules pour utiliser des websockets, dont flask-socketio). Dedans il utilise python-socketio et gunicorn (qui peut aussi servir pour utiliser Flask en production) pour construire une implémentation minimale des websockets. C'est juste ce qu'il faut pour les besoins du projet, et en plus il explique des usages plus complexes avec des sessions et de l'authentification.


### Les problèmes

#### Problèmes de version

Petites galères pour faire fonctionner gunicorn et eventlet ensemble en suivant la vidéo, car les versions de ces modules et Python 3.10 ne sont plus compatibles. J'ai donc dû installer Python 3.9 pour créer un environnement virtuel propre et installer gunicorn version 20.0.4 et eventlet version 0.30.2 (beaucoup de frustrations mais maintenant c'est bon).

D'ailleurs la commande pour lancer le serveur c'est `gunicorn -k eventlet -w 1 --reload app:app --timeout 500`, si on considère que le script pour lancer le serveur s'appelle `app.py`.

Maintenant c'est le moment de tester de combiner le programme en tkinter pour tester d'envoyer des évèments au client pour faire la partie affichage graphique.

#### Problèmes de simulation

En modifiant le code de Miguel Grinberg j'arrive à envoyer des infos au client en tapant dans le terminal du serveur et à les récupérer pour les afficher. Nickel ! J'essaie de faire ça dans une boucle `while True`, mais ça fonctionne que quand j'utilise la fonction `call`, et pas `emit`. Est-ce que c'est parce qu'avec `call` j'utilise un callback ? Et donc ça empêche qu'un appel à la fonction en cours traîne et bloque le reste du code ?

Autre possibilité : séparer le serveur et un client unique en Python qui fait tourner le code d'écoute des events et les envoie au serveur. Quand le serveur reçoit l'event il fait un `sio.emit` à tous les clients connectés. C'est ce qui se rapproche plus au niveau logique de ce que j'implémenterais ensuite avec les boutons physiques.

Dernière possibilité : repousser le problème et faire une version qui fonctionne avec des sessions de clients, avec chaque client qui peut cliquer sur un bouton, envoyer qui parle au serveur et le serveur renvoie à tous les clients connectés l'info. Permet de faire une version fonctionnelle sans les vrais boutons.

### Les solutions

La technique d'utliser un 2e programme python pour agir en tant qu client principal marche à fond, c'est parfait. Avec `client.py` on peut faire une boucle infinie dans un terminal qui demande le nom de la personne qui parle, et donc on peut simuler l'appui sur des boutons facilement. En ouvrant plusieurs fenêtres de navigateurs pour simluer des clients différent, ça marche vraiment très bien, c'est super réactif. On peut même utiliser le programme en tkinter déjà fait et ÇA MARCHE.

Ce qu'il faudrait maintenant c'est réimplémenter la logique des chronos dans tout ça. Mais maintenant on a 3 endroits pour faire les calculs :
1. Le serveur
2. Le client python
3. Le·s client.s en javascript

Puisque le client python est celui qui reçoit les inputs en premier lieu, c'est sûrement lui qui devrait faire la partie traitement des données brutes (timestamps, stockage dans la base de données) pour envoyer seulement les infos importantes au serveurs/autres clients. Il envoie un timestamp de départ/fin + le nom de la personne concernée. Le serveur doit garder le timestamp de départ du compteur global du temps de la session en cours, qu'il peut envoyer aux clients qui sont potentiellement en retard. Le js affiche la différence entre le timestamp global et le timestamp où il se connecte pour la 1e fois, et update ce temps 1 fois/seconde avec un petit `setInterval()` par exemple.

---

Pour passer du texte 00:00:00 à un objet de Timer qu'on peut mettre à jour en js, j'ai trouvé [EasyTimer.js](https://albert-gonzalez.github.io/easytimer.js/) qui va simplifier tout ça.


## Le visuel

Inspiration graphiques écconomiques, dataviz tout ça. Plus on s'éloigne du temps de parole optimal plus la couleur de la barre passe du vert au rouge. C'est possible facilement en utilisant l'espace colorimétrique hsl (teite, saturation, luminance) qui permet de changer la teinte globale avec un seul paramètre représenté par un nombre (allant de 0 à 360, un cercle chromatique).

La mise en place initiale est assez simple, mais je remarque un bug lorsqu'un·e parleur·euse dépasse 1 min de temps de parole : la barre repart à 0. C'est en fait parce que j'utilisait la fonction `timer.getTimeValues().seconds`, qui compte les secondes *affichées* (et donc de 0 à 59) et pas les secondes totales (1 min 23 correspond à 83 s). Il faut donc utiliser `timer.getTotalTimevalues().seconds`.

Pour que les animations soient fluides, puisque j'utilise en js l'event `secondsUpdated` je met 1s de transition pour la hauteur et la couleur des barres. Avec le paramètre `linear` on ne sent pas les "ticks" de l'horloge qui fait que les barres sont mises à jour toutes les secondes.

Ça rend bien mais le problème est que le site met 1 seconde avant de réagir à l'appui sur un bouton. Ce n'est pas tant que ça mais ça se ressent quand même. C'est peut-être dû au fait que je fasse les essais avec des échelles de temps assez courtes (quelques secondes) pour aller vite, mais dans une conversation le rythme sera plus long et donc les délais moins perceptibles. Une solution serait d'utiliser l'event `millisecondsUpdated` mais ça demanderait beaucoup de requêtes au client. Éventuellement je peux implémenter un throttling de 200-300 ms pour contrer ça et rendre le site plus réactif ?

### Discussions
Après discussions avec la troupe de théâtre, on a convenu qu'il serait mieux de placer les noms des personnes qui parlent au-dessus de la barre pour qu'ils soient plus visibles. Pour que ce soit plus subtil, la barre qui indiquait le temps de parole optimal a disparu, et la couleur a aussi changé. Au lieu de passer du vert (quand on a parlé une durée correcte) à rouge (quand on a beaucoup trop parlé, ou pas assez), elle passe du vert au blanc. Les parleur·euses oeuvrent collectivment pour que les barres soient les plus vertes possibles.

### Textes
Le nom de chaque participant·e s'affiche au-dessus de la barre représentant son temps de parole, mais sa taille ne change pas pour l'instant. Elle est définie en fonction du corps maximal pour que le nom le plus long rentre dans la barre la plus étroite (c'est-à-dire quand il y a les 7 personnes qui parlent/ont parlé pendant la conversation). Il serait possible de régler ça en js modifiant le corps à l'arrivée d'une nouvelle personne.

En javascript aussi il est possible de réaliser des media queries, on peut donc combiner une query sur la proportion de l'écran et des conditions selon le nombre de personnes qui parlent pour écrire les prénoms verticalement quand l'espaece disponible horizontalement est trop petit, soit parce qu'il y a trop de personnes qui parlent ou soit parce que l'écran est trop étroit, ou les 2 en même temps.

```
  let query = window.matchMedia("(min-aspect-ratio: 4/3)");

  if (query.matches) {
    verticalLimit = 5;
  } else {
    verticalLimit = 3;
  }
```

## La sauvegarde des données

La demande de ce projet est que pour chaque conversation (j'appellerais ça aussi "session" par la suite), on veut le nom des personnes qui ont participé, leur temps de parole total dans la conversation, mais aussi le moment où elles ont commencé à parler.

Pour chaque participant·e, à chaque conversation, il nous faut un tuple avec `(timestamp_départ, tps_total)`. On a donc besoin du timestamp de début et de fin de chaque prise de parole durant la conversation, ce qui nous donne une liste de tuples:

```
[
(début1, fin1),
(début2, fin2),
(début3, fin3),
(début4, fin4),
(début5, fin5),
(début6, fin6),
(début7, fin7),
(début8, fin8),
(début9, fin9),
]
```

Avec ça, au moment de l'évènement "stop" ou "reset" de la session, on peut calculer, pour chaque participant·e, le temps parlé entre chaque début et chaque fin et les additionner. On les range dans un objet associé au nom de la personne. Si quelqu'un·e ne parle pas pendant cette session, on peut simplement laisser un objet vide, qu'il faudra prendre en compte si on veut traiter les donnnées par la suite.

Ça nous permet de voir qui parle en premier, qui parle le plus longtemps, qui prend la parole le plus régulièrement, qui se fait le plus couper la parole ou qui coupe la parole le plus…

On a aussi besoin du temps total de la session, et pour ça on enregistre simplement le premier `début1` de la session et le signal de "stop" ou de "reset", et on calcule l'intervalle de temps entre les 2. S'il y a eu des pauses pendant la session, on utilise le même principe que pour les chronos individuels.

On obtiendrait pour une session donnée un objet comme ça :

```
{
    "session": (début, fin, tps_total),
    "personne1": ( [ (début1, fin1), (début2, fin2), (début3, fin3) ], tps_total )
    "personne2": ( [ (début1, fin1), (début2, fin2), (début3, fin3), (début4, fin4), (début5, fin5), (début6, fin6), (début7, fin7), (début8, fin8), (début9, fin9) ], tps_total )
    "personne3": ( [ (début1, fin1), (début2, fin2) ], tps_total  )
    "personne4": ( [ (début1, fin1), (début2, fin2), (début3, fin3), (début4, fin4), (début5, fin5), (début6, fin6) ], tps_total  )
    "personne4": ( [ (début1, fin1), (début2, fin2), (début3, fin3), (début4, fin4) ], tps_total  )

}
```

Pour donner un csv qui ressemble à ça pour une session :
```
SESSION;1678390056.961131|1678390069.09387;0:00:12.132739
bernard;1678390056.961435|1678390058.01827,1678390064.645609|1678390065.862688,1678390066.86447|1678390069.09469;0:00:04.504134
charlotte;1678390058.019189|1678390059.316452,1678390060.319152|1678390063.460415;0:00:04.438526
francine;1678390059.316949|1678390060.317929;0:00:01.000980
alexandra;1678390063.460849|1678390064.645096;0:00:01.184247

```

On a une personne (ou le chrono global) pour chaque ligne, avec 3 colonnes : le nom, la liste des timestamps de début et de fin de chaque intervention, et le temps total parlé.

## Sur le Raspberry Pi

Il y a moyen d'utiliser Firefox en mode --kiosk pour ne pas afficher l'interface et avoir ainsi un vrai plein écran pour afficher le graphe.

## euh (un lexique)

+ DAC → Digital to Analog Converter
+ HAT → Hardware Attached on Top (extension RPi)
+ Neutrik → Un type de connecteur XLR (apparement les meilleurs) qui isole bien des interférences (mais on a pas besoin d'autant de précision je pense)
+ Phantom power / alimentation fantôme → propriété de certains câbles (dont les XLR) d'alimenter des micros sans alimentation externe
+ gunicorn -k eventlet -w 1 --reload app:app --timeout 500
