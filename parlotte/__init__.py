#!/usr/bin/env python

### MODULES IMPORTÉS ###

import time
from tkinter import *
import datetime as dt
import shelve

### LES VARIABLES GLOBALES DE DATE ###

jourheure = str(dt.datetime.now())
ext_jourheure = jourheure[:10] + "_" + jourheure[11:16]

### LES PARLEURS ET LES PARLEUSES ###

# ~speakers = ["Selma", "Alexandra", "Francine", "Rachel", "Bernard", "Miguel", "Stéphane"]
# ~speakers = ["Selma", "Alexandra", "Francine", "Bernard", "Miguel", "Stéphane"]
speakers = [
    "Selma",
    "Alexandra",
    "Francine",
    "Charlotte",
    "Bernard",
    "Miguel",
    "Stéphane",
]

### LES CLASSES ###


class Chrono(Frame):
    """Structure de chronomètre, avec le nom de la personne, la durée, et le bouton go/stop"""

    def __init__(self, parent, qui="Quelqu'un", running=False):
        Frame.__init__(self, bg="ivory", bd=3, relief=GROOVE)
        self.parent = parent
        self.qui = qui
        self.running = running
        self.counter = 0
        # Nom de l'intervenant·e
        self.nom = Label(
            self,
            text=self.qui,
            fg="darkblue",
            bg="ivory",
            # font="Arial 20",
            font="Inconsolata 20",
            padx=5,
            pady=5,
        )
        self.nom.pack(side="top")
        # Affichage du temps
        self.temps = Label(
            self,
            text="motus",
            height=1,
            width=9,
            fg="black",
            bg="ivory",
            font="Arial 25 bold"
            # ~font="Inconsolata 25 bold"
        )
        self.temps.pack(side="top")
        # Bouton départ/arrêt chronomètre
        self.bouton = Button(
            self,
            # ~text='Start/Stop',
            text="START",
            fg="black",
            # ~bg="coral",
            bg="Sandy Brown",
            command=lambda: self.start_stop(),
        )
        self.bouton.pack(side="top", padx=5, pady=5)

    def count(self):
        if self.running == True:
            if self.counter == 0:
                display = "PARLE"  # Je le garde pour si jamais je veux le remettre plus tard > un signal
                # ~display = self.lancement_temps()

                # RAJOUT
                global sio
                sio.emit("qui_parle", self.qui)
            else:
                display = self.lancement_temps()
                self.temps["fg"] = "FireBrick"

            self.temps["text"] = display  # Or temps.config(text=display)
            # ~self.temps['fg'] = 'black'
            # ~self.temps['fg'] = 'dark red'
            self.temps.after(1000, self.count)
            self.counter += 1

    def lancement_temps(self):
        tt = dt.datetime.fromtimestamp(self.counter - 3600)
        string = tt.strftime("%H:%M:%S")
        display = string
        return display

    def start_counter(self):
        self.running = True
        self.count()
        self.bouton["text"] = "STOP"
        self.temps["fg"] = "FireBrick"
        self.stop_all_others()

    def stop_counter(self):
        self.running = False
        self.bouton["text"] = "START"
        # ~self.temps['text'] = 'se tait'
        # ~self.temps['fg'] = 'dimgrey'
        self.temps["fg"] = "black"

    def start_stop(self):
        if self.running is False:
            self.start_counter()
        else:
            self.stop_counter()

    def stop_all_others(self):
        for i in self.parent.chronos:
            if self.parent.chronos[i].qui != self.qui:
                self.parent.chronos[i].running = False
                self.parent.chronos[i].stop_counter()


class ChronoGlobal(Frame):
    """Classe spécifique pour le chronomètre global"""

    def __init__(self, parent, qui="Discussion", running=True):
        Frame.__init__(
            self,
            # ~bg='ivory',
            bg="grey70",
            bd=3,
            # ~relief=GROOVE
            relief=RIDGE,
        )
        self.parent = parent
        self.qui = qui
        # un compteur par chronomètre
        self.running = running
        self.counter = 0
        # Affichage discussion
        self.nom = Label(
            self,
            text=self.qui,
            fg="purple",
            # ~bg='ivory',
            bg="grey70",
            # font="Arial 20",
            # font="LucidaConsole 20",
            font="Inconsolata 20 bold",
            padx=5,
            pady=5,
        )
        self.nom.pack(side="top")
        # Affichage du temps
        self.temps = Label(
            self,
            # ~text="Rien",
            height=1,
            width=9,
            # ~fg="dim grey",
            # ~fg='purple',
            # ~bg='ivory',
            # ~bg='grey',
            bg="grey70",
            font="Arial 25 bold"
            # ~font="Inconsolata 25 bold"
        )
        self.temps.pack(side="top")
        # Bouton marche/arrêt chronomètre
        self.bouton = Button(
            self,
            text="Start/Stop",
            # ~text='START',
            fg="black",
            # ~bg="coral",
            bg="Sandy Brown",
            # height=5,
            width=10,
            command=lambda: self.start_stop(),
        )
        self.bouton.pack(side="top", padx=5, pady=5)
        self.count()

    def count(self):
        if self.running == True:
            display = self.lancement_temps()
            self.temps["text"] = display  # Or temps.config(text=display)
            # ~self.temps['fg'] = 'black'
            # ~self.temps['fg'] = 'grey70'
            self.temps["fg"] = "white"
            self.temps.after(1000, self.count)
            self.counter += 1

    def lancement_temps(self):
        # ~tt = datetime.fromtimestamp(self.counter-3600) #workaround retirer une heure pour arriver à 0; pourquoi est-ce qu'il part à 01:00:00??
        tt = dt.datetime.fromtimestamp(self.counter - 3600)
        string = tt.strftime("%H:%M:%S")
        display = string
        return display

    def start_counter(self):
        self.running = True
        self.count()
        self.bouton["text"] = "STOP"

    def stop_counter(self):
        self.running = False
        self.bouton["text"] = "START"

    def start_stop(self):
        if self.running == False:
            self.start_counter()
        else:
            self.stop_counter()


class Tableau(Frame):
    """Fenêtre principale qui englobe les chronomètres"""

    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.parent.title("Parlotte équitable")

        self.chronos = {}
        for speaker in speakers:
            self.chronos[speaker] = Chrono(self, speaker, False)
        self.chronoglobal = ChronoGlobal(parent, qui="Discussion", running=True)

        # ~f = Frame()

        #  Bouton remise à zéro des conversations ## À voir si nécessaire plus tard / pas opérationnel de toute façon
        # ~self.remazero = Button(f, text="Remise\nà zéro", fg="purple", font="Inconsolata 15 bold", command=self.remise_a_zero) # Pas au point, comprens pas pourquoi
        # ~self.remazero.pack(anchor=S, padx=5, pady=5)
        # ~self.remazero.grid(row=2)

        # Discussion
        # ~self.chronoglobal.pack(f, padx=5, pady=5)

        # Bouton export
        self.export = Button(  # f,
            text="Exporter",
            fg="dark green",
            font="Inconsolata 15 bold",
            command=self.export,
        )
        # ~self.export.pack(anchor=N, padx=5, pady=5)

        # Bouton quitter
        self.terminer = Button(  # f,
            text="Quitter", fg="red", font="Inconsolata 15 bold", command=self.quit
        )
        # ~self.terminer.pack(anchor=S, padx=5, pady=5)
        # ~self.terminer.grid(row=2, column=3)

        # Trouver un moyen de les aligner automatiquement
        self.chronos["Selma"].grid(column=0, row=0, padx=5, pady=5)
        self.chronos["Alexandra"].grid(column=1, row=0, padx=5, pady=5)
        self.chronos["Francine"].grid(column=2, row=0, padx=5, pady=5)
        self.chronos["Charlotte"].grid(column=3, row=0, padx=5, pady=5)
        # ~self.chronoglobal.grid(column=3, row=0, padx=5, pady=5)
        self.chronos["Bernard"].grid(column=0, row=1, padx=5, pady=5)
        self.chronos["Miguel"].grid(column=1, row=1, padx=5, pady=5)
        self.chronos["Stéphane"].grid(column=2, row=1, padx=5, pady=5)

        self.chronoglobal.grid(column=3, row=1, padx=5, pady=5)

        self.export.grid(column=1, row=2, padx=5, pady=5)
        self.terminer.grid(column=2, row=2, padx=5, pady=5)
        # ~f.grid(column=3, row=1, padx=5, pady=5)
        # ~f.grid(column=2, row=3, padx=5, pady=5)
        # ~f.grid(column=0, row=2, padx=5, pady=5)
        # ~f.grid(column=1, row=2, padx=5, pady=5)
        # ~self.remazero.grid(column=3, row=1, padx=5, pady=5)
        # ~self.terminer.grid(column=4, row=1, padx=5, pady=5)

    def remise_a_zero(self):  # pas encore opérationnel
        for chrono in self.chronos:
            if self.chronos[chrono].running == True:
                # ce n'est pas ça qu'il faut, c'est réinitialiser le compteur, partir de tt fromtimestamp etc., en faire une méthode séparée réutilisable ici. Mais d'abord il faut arrêter tous les compteurs.
                self.chronos[chrono].running = False
            # ~display = self.chronos[chrono].lancement_temps()
            display = "00:00:00"  # Ne marche pas, ça reprend après où ça l'a laissé
            # ~self.chronos[chrono].temps.config(text=display)
            self.chronos[chrono].count()

    def export(self):
        # + ext_annee ## pas la peine de rajouter l'année si elle est déjà dans l'évènement
        self.nom_db = "CODA_Discussion_" + ext_jourheure
        db = shelve.open(self.nom_db)
        for s in speakers:
            db[s] = self.chronos[s].temps["text"]
        db[self.chronoglobal.qui] = self.chronoglobal.temps["text"]
        db.close()


# ~def main(args):
# ~return 0


if __name__ == "__main__":
    root = Tk()
    # ~root.geometry('800x600')
    tab = Tableau(parent=root)
    # ~tab.creer_chronos
    # ~print("Les chronos sont créés normalement")
    tab.mainloop()
    # ~root.mainloop()
    # ~import sys
    # ~sys.exit(main(sys.argv))
