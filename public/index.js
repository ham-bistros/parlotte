// DÉFINITIONS

function capitalize(string) {
	return string[0].toUpperCase() + string.substring(1);
}

function firstSpeak(nom) {
	document.getElementById(nom).style.display = "grid";

	numberSpeakers += 1;
	timers[nom] = new easytimer.Timer({ precision: "secondTenths" });
	// console.log(`euh zou c'est ${nom} qui rentre dans la conversation`);

	const docRoot = document.querySelector(":root");
	docRoot.style.setProperty("--max-speakers", numberSpeakers);

	timers[nom].start();

	timers[nom].addEventListener("secondsUpdated", (e) => {
		document.querySelector(`#${nom} .time`).innerHTML = timers[nom]
			.getTimeValues()
			.toString();
});

timers[nom].addEventListener("secondTenthsUpdated", (e) => {
	totalSpokenTime += 1;
});

timers[nom].addEventListener("started", (e) => {
	document.querySelector(`#${nom} .time`).innerHTML = timers[nom]
		.getTimeValues()
		.toString();
});

timers[nom].addEventListener("reset", (e) => {
	document.querySelector(`#${nom} .time`).innerHTML = timers[nom]
		.getTimeValues()
		.toString();
});

speakersCheck();
}

function clamp(value, min, max) {
	return Math.max(min, Math.min(value, max));
}

function stopParle(nom, func) {
	if (func == "pause") {
		console.dir(timers);
		console.log(`${nom} PAUSE`);
		timers[nom].pause();

	} else if (func == "stop") {
		console.log(`${nom} STOP`);
		timers[nom].stop();
	}
	const speakerName = document.querySelector(`#${nom} .name`);
	speakerName.classList.remove("speaking");
}

function updateBars() {
	const optimalTime = totalSpokenTime / numberSpeakers;

	for (let nom in timers) {
		const spokenTime = timers[nom].getTotalTimeValues().secondTenths;
		const percent = (spokenTime / totalSpokenTime) * 100;

		const distance =
			(spokenTime - optimalTime) / (totalSpokenTime - optimalTime);
		const barre = document.querySelector(`#${nom} .barre`);
		barre.style.height = `${percent}%`;

		// Avec 100 on contraste les couleurs 2 fois plus vite
		// Avec 200 on contraste les couleurs 4 fois plus vite
		const luminance = Math.abs(distance) * 800;
		const opacite = 1 - Math.abs(distance * 8);

		// Plus la distance est grande plus la couleur de la barre tire vers le blanc
		// On peut ajouter une couleur perso pour chaque parleur·euse
		if (Object.keys(timers).length > 1) {
			barre.style.setProperty("--local-lum", `${clamp(luminance, 50, 100)}%`);
		}

		barre.style.opacity = `${clamp(opacite, 0.5, 1)}`;
		barre.querySelector(`.pourcent`).innerHTML = `${Math.floor(percent)}%`;

	}
}

function ratioCheck() {
	let query = window.matchMedia("(min-aspect-ratio: 1/1)");

	if (query.matches) {
		verticalLimit = 5;
	} else {
		verticalLimit = 3;
	}
	speakersCheck();
}

function speakersCheck() {
	if (numberSpeakers >= verticalLimit) {
		graph.classList.add("vertical");
	} else {
		graph.classList.remove("vertical");
	}
}

function toggleDisplayWidth() {
    console.log("TOGGLE AFFICHAGE");
    document.body.classList.toggle("mobile");
}

// SOCKETIO
const sio = io();

sio.on("connect", () => {
	console.log("connected");
});

sio.on("connect_error", (e) => {
	console.log(e.message);
});

sio.on("disconnect", () => {
	console.log("disconnected");
});

sio.on("client_count", (count) => {
	console.log(`There are ${count} connected clients.`);
});

sio.on("speaker_name", (nom) => {
	if (numberSpeakers == 0) {
		globalTimer = new easytimer.Timer({ precision: "secondTenths" });
		globalTimer.start();

		globalTimer.addEventListener("secondsUpdated", () => {
			document.getElementById("total-time").innerHTML = globalTimer
				.getTimeValues()
				.toString();
		});

		globalTimer.addEventListener("secondTenthsUpdated", () => {
			const now = Math.floor(Date.now() / 100);
			// console.log(now - lastUpdate);

			if (now - lastUpdate >= 3) {
				updateBars();
				lastUpdate = now;
				// console.log("updated");
			}

		});

		globalTimer.addEventListener("reset", () => {
			document.querySelector("total-time").innerHTML = globalTimer
				.getTimeValues()
				.toString();
		});
	}

	if (globalTimer.isPaused()) {
		globalTimer.start();
	}

	const person = document.getElementById(nom);
	if (person.style.display == "" || person.style.display == "none") {
		firstSpeak(nom);
	} else {
		timers[nom].start();
	}

	const speakerName = document.querySelector(`#${nom} .name`);
	speakerName.classList.add("speaking");
});

sio.on("stop_parle", (nom) => {
	stopParle(nom, "pause");
});

sio.on("pause_session", () => {
	console.log("session paused");
	globalTimer.pause();
	for (let nom in timers) {
		stopParle(nom, "pause");
	}
});

sio.on("stop_session", () => {
	console.log("session stopped");
	globalTimer.stop();
	for (let nom in timers) {
		stopParle(nom, "stop");
	}
});

sio.on("reset", () => {
	console.log("------------------- RESET ------------------");

	// si c'est la première initialisation globalTimer ne sera pas défini
	try {
		// globalTimer.reset();
		globalTimer.pause();
		globalTimer.stop();
	} catch (error) {
		if (error instanceof TypeError) {
			console.log("(globalTimer pas encore défini mais c'est ok)");
		} else {
			console.error(error, "pagrave ;)");
		}
	}

	for (let nom in timers) {
		timers[nom].stop();
	}

	timers = {};

	const totalTime = document.getElementById("total-time");
	totalTime.innerHTML = "00:00:00";

	const barres = document.getElementsByClassName("wrapper");
	for (let wrapper of barres) {
		wrapper.style.display = "none";
		wrapper.querySelector(".time").innerHTML = "00:00:00";
		wrapper.querySelector(".pourcent").innerHTML = "0%";

		const barre = wrapper.querySelector(".barre");
		barre.style.opacity = "1";
		barre.style.height = "0%";
		barre.style.setProperty("--local-lum", "100%");
	}


	numberSpeakers = 0;
	totalSpokenTime = 0;
});

// LES TRUCS GLOBAUX
let timers = {};
const speakers = [
	"alexandra",
	"bernard",
	"charlotte",
	"francine",
	"miguel",
	"sara",
	"stéphane",
];

let lastUpdate = 0;

let globalTimer;
let totalSpokenTime = 0;
let numberSpeakers = 0;
const graph = document.getElementById("graph");
let verticalLimit;

ratioCheck();

window.addEventListener("resize", ratioCheck);

// TOGGLE AFFICHAGE PHONE / RESPONSIVE
window.addEventListener("keypress", (ev) => {
    if (ev.keyCode == 116) {
        // t
        toggleDisplayWidth();
    };
});
