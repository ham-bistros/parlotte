import RPi.GPIO as GPIO
import time

# Define pin mode : BCM vs physical (BCM works on all rpi gen)
GPIO.setmode(GPIO.BCM)

# Define GPIO pins - [0] is not used so that button 1 = input_pin[1]
input_pins = [0, 21, 22, 23, 24, 25, 26, 27, 18]
button_pressed = [False] * len(input_pins)

# set GPIO pins as inputs with pull-up resitors
# pull-up means that input is always True except when pressed (sinked)
GPIO.setup(input_pins, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# Loop
while True:
    input_status = []  # reset of array at each cycle
    # Reads each input pin and stores in array
    for pin in input_pins:
        input_status.append(GPIO.input(pin))

    # input_status[i] will be 0 if button i is pressed
    for i in range(len(input_status)):
        if input_status[i] == 0 and not button_pressed[i]:  # 0 = triggered
            print(f"Button number {i} is pressed")
            # memory that it has been pressed so print is done only once
            button_pressed[i] = True
        elif input_status[i] == 1:
            # button off or released
            button_pressed[i] = False

    # add a short dealy to reduce CPU usage
    # if delay to high -> miss detection
    time.sleep(0.2)

# Clean up GPIO pins
GPIO.cleanup()
