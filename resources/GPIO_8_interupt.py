import RPi.GPIO as GPIO
import time


# define interup function callback
def my_callback(channel):
    print(f"Button number {channel} is pressed")


# Define pin mode : BCM vs physical (BCM works on all rpi gen)
GPIO.setmode(GPIO.BCM)

# Define GPIO pins - [0] is not used so that button 1 = input_pin[1]
input_pins = [21, 22, 23, 24, 25, 26, 27, 18]

# set GPIO pins as inputs with pull-up resitors
# pull-up means that input is always True except when pressed (sinked)
GPIO.setup(input_pins, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# GPIO.FALLING (or RISING or BOTH) will call my_callback if it detects a falling edge (1 -> 0)
for pin in input_pins:
    GPIO.add_event_detect(pin, GPIO.FALLING, callback=my_callback, bouncetime=200)

# Loop
while True:
    # add a short dealy to reduce CPU usage
    time.sleep(3)

# Clean up GPIO pins
GPIO.cleanup()
