#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parlote.py
#
#  Copyright 2022 Miguel <miguel@yorick>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

##########################
### CAHIER DES CHARGES ###
##########################

# 13/12/2022
# Essai de développement de compteurs de temps de paroles divers (pour CODA principalement)
# Autant de chronos que de participants > implique la possibilité d'en générer dans la même interface.
# Chaque fois qu'on clique sur un chrono, il est activé et les autres s'arrêtent.
# Chaque chrono garde le temps en mémoire et l'affiche > un bouton marche/arrêt et un afficheur.
# Un bouton spécial global pour arrêter tous les chonomètres.

# Pour une application en standalone: py2app !

# 1) Je vais commencer par créer un chrono en terminal.
# 2) Je vais créer une fenêtre avec un chrono unique.
# 3) Je vais les encapsuler dans une fenêtre globale.

# fonctions nécessaires:
# Start lancer
# Arrêt
# comptabiliser et enregistrer le laps de temps écoulé
# repartir (start)
# à l'arrêt enregistrer le laps écoulé et additionné avec le temps global

# Là, le compteur compte toujours. Quand j'appuie enter, il ne me donne qu'une idée. Pour qu'il arrête de compter, il faut
# 1. qu'il arrête de compter
# 2. qu'il enregistre là où il en est

# Bon je pars de choses qui existent déjà.

# code ci-dessous de <https://stacklima.com/creer-un-chronometre-a-laide-de-python/>

# Mais je ne comprends pas pourquoi j'ai toujours un 01:00:00 au départ. Et pourquoi je dois utiliser datetime. Parce que j'ai l'impression que ça vient de datetime.fromtimestamp. Je voudrais pouvoir partir du moment où on lance le compteur, est-ce que je ne peux pas faire ça avec time (et pas datetime)?
# De ceci <https://www.developpez.net/forums/d559259/autres-langages/python/general-python/chronometre-python/> je peux m'inspirer pour avoir un tZero à partir duquel travailler.
# Mais alors c'est un autre système que par un compter qui s'incrémente toutes les secondes. Ce serait un while running:
# Si c'est le temps, et pas un compteur, alors il faut réinitialiser sur un nouveau tZero, mais tout en gardant en mémoire le temps. Ce que j'ai est pas mal, il faut juste enlever 3600 secondes.

# J'ai un élément qui fonctionne. Il faut maintenant créer un cadre général, et multiplier ce que j'ai par 7.
# Et l'enjeu, c'est que un start sur un autre compteur coupe tous les autres.
# Le code n'est pas très propre, je pense qu'il faudrait encapsuler les fonctions dans des classes.

# 15/12/2022
# Définir une classe Chrono et une classe Tableau qui les contienne. Revoir ce que j'avais fait déjà pour IDIO et pour PSHO.
# Essayer aussi que le label du temps soit le label du bouton, pour qu'au final il n'y ait que les 7 boutons à cliquer qui affichent les temps écoulés.

# 17/12/2022
# Il faut aussi un bouton qui stoppe tout quand on interrompt la conversation, si on veut la reprendre plus tard. Ce bouton partagerait la même méthode que pour couper les chronos quand quelqu'un se met à parler, mais il toucherait tout le monde (mettre une sorte d'exception quand on parle).

# 20/12/2022
# Mes essais de créer des classes ne sont pas concluants. Je restaure ici ce qui fonctionne et qui a été importé d'un site cité plus haut. Je continue dans tpsparole_labo.py pour des essais avec les classes.

# Beaucoup de problèmes.
# Ici essais avec les classes.
# La base de l'appli devrait plutôt être un frame, plus simple. Canvas peut le faire aussi, mais c'est moins nécessaire.
# Essayer petit à petit.

# 22/12/2022
# Idée pour les chronos: que le bouton soit nommé par nom de la personne, qui s'affiche en vert s'il parle ou en rouge s'il ne parle pas
# Idées: un petite barre de progression qui montre où chacun·e en est de la conversation générale
# Possibilité d'arrêt global de la conversation et de reprise
# Possibilité de mise à zéro
# Export dans une db récupérable
# Export en format csv de chaque session

# 24/12/2022
# Choses qui marchent:
# - les chronos, marche et arrêt
# - plusieurs chronos.
# Choses qui ne marchent pas encore:
# - la disposition est complètement foireuse. La hiérarchie des spécifications doit être mauvaise.
# À faire:
# - Que le start_counter d'un des chronos intervienne sur tous les autres en déclenchant un stop_counter (est-ce qu'il ne faut pas alors que ce soient des fonctions globales?).

# 25/12/2022
# Cahier des charges plus formel
# ------------------------------
# Un tableau général qui comporte autant de chronomètres qu'il y a de participants à la conversation.
# On peut les instancier en entrant au préalable une liste de participants, liste qui peut être elle-même nommée à son tour, et récupérée pour d'autres occasions.
# Chaque chronomètre s'inscrit dans le tableau -- idéalement selon une grille qui s'organise de la manière la plus compacte possible (par ex. 3 éléments en largeur sur 2 en hauteur avec centrage des éléments inférieurs...)
# Chaque chronomètre est constitué de trois parties: le nom de la personne qui parle, le temps écoulé pendant lequel elle a pris la parole, et le bouton activant son chronomètre (plus tard barre de progression, etc., cf. plus haut).
# Chaque fois qu'un chrono est activé, tous les autres (c'est-à-dire surtout celui qui était actif) sont désactivés. Mais si on clique sur le chrono qui était actif, il s'arrête, et cela marque une interruption dans la conversation, tous les chronos étant à l'arrêt.
# Par la suite, on pourra rajouter diverses indications, comme la proposition de temps de parol de chacun dans la conversation générale (ce qui implique d'additionner tous les temps constamment, à chaque seconde écoulée dans le chrono de cellui qui parle).

# 26/12/2022
# Ce qui marche:
# - l'alignement des chronos, et les bordures pour les délimiter
# Ce qui ne marche pas:
# - la taille de chacun des chronos
# - les commandes pour identifier le nouveau locuteur (j'ai essayé avec une variable "parle")
# - les commandes pour arrêter les autres.
# Il faudrait un flag parle, qui serait booléen.

# 27/12/2022
# Une classe maitresse des Chronos, par ex. Conversation? qui aurait le loisir de commander les attributs de ses instances dépendantes.
# Ce qui manque, c'est au niveau maitre des chronos la détection d'un *nouveau* parle.
# Maitre doit avoir un groupe de parle de chaque chrono qui soit True ou False. Trouver une formule pour qu'il n'y ait qu'un seul parle.
# Pseudo-code:
# for i, j in conversation => la liste des parle
#   while parle[i] == True:
#       for [j] != i:
#           parle[j] = False
# En fait, est-ce que parle est différent de running? Pas que je voie. Mais ça pourrait être un des attributs de chaque instance de Chrono.
# Je déplace la méthode dans Chrono, qui est self, et qui va chercher dans le dictionnaire d'Application. Donc c'est le chrono qui change qui déclenche l'intervention chez les autres. Mais l'identification ne marche pas. Je vais peut-être remplacer qui par id, c'est peut-être une valeur par défaut.
# Il faut que je retrouve comment appeler l'attribut d'un objet.

# 28/12/2022
# id est une fonction spéciale qui retourne l'identifiant unique d'un objet. Je crains que ça complique les affaires. Je remets qui.
# Ça marche. La syntaxe correcte:
#        for i in self.parent.chronos:
#            if self.parent.chronos[i].qui != self.qui:
#                self.parent.chronos[i].running = False

# J'ai rajouté une ébauche de boutons supplémentaires pour une remise à zéro, mais c'est juste un arrêt global. Je n'en ai pas besoin, il suffit d'appuyer sur le bouton de celui qui parlait pour qu'il s'arrête, et tout le monde est arrêté.
# (Peut-être que ce serait pas mal d'avoir un compteur général de la conversation, idépendamment des silences éventuels.)
# Pour la mise en page, je ne comprends pas pourquoi les spécifications de taille dans Frame.__init__ du Chrono ne marchent pas, alors que tout le reste bien. Mais ce n'est pas grave, j'ai diminué la police.

# 29/12/2022
# J'ai essayé de créer une remise à zéro correcte, mais ça ne marche pas, ça rajoute juste une seconde aux derniers chronos actifs. C'est très curieux, je ne comprends pas bien pourquoi.
# Mais j'essaye de réfléchir à la mise en place. Là je les ai tous sur une bande. Ce qui est curieux, c'est que je mets les boutons en BOTTOM et les chronos en TOP, et ce sont les boutons qui sont en haut et les chronos en-dessous.
# Je me demande si je ne les mettrais pas dans des frames avec des dimensions précises, dans lesquels les chronos viendraient s'inscrire. Ou alors un grid. Mais un grid ne va pas me permettre d'être fluide, ou alors il faut faire un calcul du nombre de chronos, et déduire la meilleure disposition, pour arriver à un carré. Si c'est 4, le mieux sera 2 et 2. Si c'est 5, 3 sur 2. Si c'est 6, 3 et 3. 7, 3, 3 et 1, ou 4 et 3? 8, 3, 3, 2, ou 4 et 4? Non, 3. C'est en fait la racine carrée du nombre dont c'est le carré qui est le prochain plus grand. Ça me permettrait de créer une formule pour déterminer le grid.
# Mais en attendant, je peux juste le faire à la main.
# Ok, essayé avec grid, mais c'est compliqué, les chronos ne peuvent pas être en pack. Il faut tout refaire à la main.
# Mais il y a le problème des grid à l'intérieur de grid. Le mieux alors c'est de travailler avec des frames différents. Cf. les marque-pages dans firefox.

# 07/01/2023
# Je ne m'en sors pas vraiment dans l'instanciation en grid de tous les chronos. Qu'est-ce qui instancie quoi, entre le frame de l'application et le tk de base.
# Tableau instancie les chronos. Application instancie le tableau.
# Pour le moment, je n'ai qu'un tableau vide.
# Voilà je m'en sors. La place des boutons n'est pas encore évidente.
# Pour lundi, il manque encore l'export dans une db, et l'affichage.
# Pour la barre de progression, aller voir dans les ttk, les themed widgets, il y en a plus.
# Mais il faut d'abord que je puisse stocker les valeurs de temps. Or ce n'est pas exactement ce que j'ai. Prendre les chaines de caractères et les convertir en valeur numériques, mais en base 60.
# Et puis automatiser aussi le placement dans le grid. J'ai de quoi dans les marque-pages.

# 08/01/2023
# Je m'essaye à l'export des données de conversation en format csv. En fait, il s'agit juste de recopier
# Il faut aussi un compteur global de la conversation. Ça devrait être facile.
# Dans Think Python il y avait des manipulations de base qui additionnaient simplement des temps sans devoir passer par time et datetime, ce qui est peut-être plus simple pour ce dont j'ai besoin pour le moment.
# Donc:
# -- trouver un moyen de récupérer les strings des labels de temps
# -- les ajouter dans un dictionnaire (après, une db), avec le nom en clé et la durée en valeur
# -- ajouter aussi la durée globale de la conversation
# Si elle a des blancs, la durée totale ne sera pas la même que la durée globale (à moins que je trouve un moyen d'arrêter le compte global quand il y a une interruption, mais à priori ça pourrait être intéressant d'avoir aussi la proportion de blanc dans la conversation.
# Le compteur de conversation se lance dès le premier qui parle.
# Pour avoir le compteur de la discussion, j'ai voulu commencer par faire une dérivation par héritage de la classe Chrono, mais c'était trop compliqué (j'avais les labels et boutons de Chrono, + ceux que j'avais modifiés en dessous, et pas le bouton). J'ai refait une classe autonome, en ne prenant que ce qui m'intéressait.
# Reste à faire l'export.

# Pour l'export: il faut d'abord créer le dictionnaire (ou la db), avec les noms («qui») de tous les membres du dictionnaire de départ, et rajouter la discussion.
# Pseudo-code:
# Ça doit être dans Tableau
# une méthode export
# def export
# data = {}
# for speaker in speakers:
# data[speaker] = self.chronos[speaker].label['text']
# data[discussion] = self.chronoglobal.label['text']
# Et créer un bouton pour lancer la commande

# Mettre un bouton stop à la discussion aussi.

# L'export en db fonctionne, en appuyant sur le bouton export.
# Mais le chrono de la discussion continue à tourner, et peut-être bien aussi la personne qui parle. Il faut rafiner un peu. Peut-être que l'export freeze tout, et envoie un signal stop au compteur discussion.
# Reste à voir les calculs pour l'export en csv, et éventuellement aussi la barre de progression individuelle. Mais on peut déjà partir de la db pour créer un fichier csv.

# 11/01/2023
# Ça aiderait par exemple que ce soit affiché sur un écran supplémentaire. Que ça pourrait être piloté par un raspberry, par des boutons. Est-ce qu'on a encore un contrôleur avec des boutons, qui serait connectable par usb? Ou alors par un clavier numérique? Ce serait plus facile qu'avec la souris.
# Faire un graphe évolutif avec des barres différentes selon chacun des locuteurs. Question: plus ça va, plus ça augmente. Quelle est la limite? Est-ce quand ça augmente, ça reconditionne l'aspect général du graphe (barres plus larges individuellement quand elles touchent le plafond? plus fines collectivement en forme de zoom out? En cercle qui contiendrait la conversation dans sa totalité?)

# 14/01/2023
# Pour le problème que je ne sais pas comment anihiler une méthode dans une classe dérivée: créer une classe générale plus minimaliste, et en faire deux dérivées avec la méthode en plus pour les chronos locaux, et d'autres (si nécessaire) pour la chrono globale (peut-être même que ce serait la globale la classe de base).

# 26/01/2023
# Les classes dérivées apportent plus de problèmes, c'est plus clair à manipuler, même si moins élégent, quand ce sont des classes complètement séparées. Elles ont aussi des logiques légèrement différentes.
# Le bouton stop de discussion devrait être lié aux autres conversations. Est-ce que dès que plus personne ne parle, la discussion se coupe automatiquement? Ou bien est-ce qu'on veut garder la proportion de blanc qu'il y a eu dans la discussion? Je ne sais pas. Je me dis que ce serait bien de le garder, mais si par exemple on fait autre chose que discuter, ce serait bien de le couper.
# Ce qui serait bien, c'est que le chrono Discussion se remette en marche dès qu'un chrono se remet en marche.

#################
### PROGRAMME ###
#################

### MODULES IMPORTÉS ###

import time
from tkinter import *
import datetime as dt
import shelve

### LES VARIABLES GLOBALES DE DATE ###

jourheure = str(dt.datetime.now())
ext_jourheure = jourheure[:10] + "_" + jourheure[11:16]

### LES PARLEURS ET LES PARLEUSES ###

# ~speakers = ["Selma", "Alexandra", "Francine", "Rachel", "Bernard", "Miguel", "Stéphane"]
# ~speakers = ["Selma", "Alexandra", "Francine", "Bernard", "Miguel", "Stéphane"]
speakers = [
    "Selma",
    "Alexandra",
    "Francine",
    "Charlotte",
    "Bernard",
    "Miguel",
    "Stéphane",
]

### LES CLASSES ###


class Chrono(Frame):
    """Structure de chronomètre, avec le nom de la personne, la durée, et le bouton go/stop"""

    def __init__(self, parent, qui="Quelqu'un", running=False):
        Frame.__init__(self, bg="ivory", bd=3, relief=GROOVE)
        self.parent = parent
        self.qui = qui
        self.running = running
        self.counter = 0
        # Nom de l'intervenant·e
        self.nom = Label(
            self,
            text=self.qui,
            fg="darkblue",
            bg="ivory",
            # font="Arial 20",
            font="Inconsolata 20",
            padx=5,
            pady=5,
        )
        self.nom.pack(side="top")
        # Affichage du temps
        self.temps = Label(
            self,
            text="motus",
            height=1,
            width=9,
            fg="black",
            bg="ivory",
            font="Arial 25 bold"
            # ~font="Inconsolata 25 bold"
        )
        self.temps.pack(side="top")
        # Bouton départ/arrêt chronomètre
        self.bouton = Button(
            self,
            # ~text='Start/Stop',
            text="START",
            fg="black",
            # ~bg="coral",
            bg="Sandy Brown",
            command=lambda: self.start_stop(),
        )
        self.bouton.pack(side="top", padx=5, pady=5)

    def count(self):
        if self.running == True:
            if self.counter == 0:
                display = "PARLE"  # Je le garde pour si jamais je veux le remettre plus tard > un signal
                # ~display = self.lancement_temps()
            else:
                display = self.lancement_temps()
                self.temps["fg"] = "FireBrick"
            self.temps["text"] = display  # Or temps.config(text=display)
            # ~self.temps['fg'] = 'black'
            # ~self.temps['fg'] = 'dark red'
            self.temps.after(1000, self.count)
            self.counter += 1

    def lancement_temps(self):
        tt = dt.datetime.fromtimestamp(self.counter - 3600)
        string = tt.strftime("%H:%M:%S")
        display = string
        return display

    def start_counter(self):
        self.running = True
        self.count()
        self.bouton["text"] = "STOP"
        self.temps["fg"] = "FireBrick"
        self.stop_all_others()

    def stop_counter(self):
        self.running = False
        self.bouton["text"] = "START"
        # ~self.temps['text'] = 'se tait'
        # ~self.temps['fg'] = 'dimgrey'
        self.temps["fg"] = "black"

    def start_stop(self):
        if self.running == False:
            self.start_counter()
        else:
            self.stop_counter()

    def stop_all_others(self):
        for i in self.parent.chronos:
            if self.parent.chronos[i].qui != self.qui:
                self.parent.chronos[i].running = False
                self.parent.chronos[i].stop_counter()


class ChronoGlobal(Frame):
    """Classe spécifique pour le chronomètre global"""

    def __init__(self, parent, qui="Discussion", running=True):
        Frame.__init__(
            self,
            # ~bg='ivory',
            bg="grey70",
            bd=3,
            # ~relief=GROOVE
            relief=RIDGE,
        )
        self.parent = parent
        self.qui = qui
        # un compteur par chronomètre
        self.running = running
        self.counter = 0
        # Affichage discussion
        self.nom = Label(
            self,
            text=self.qui,
            fg="purple",
            # ~bg='ivory',
            bg="grey70",
            # font="Arial 20",
            # font="LucidaConsole 20",
            font="Inconsolata 20 bold",
            padx=5,
            pady=5,
        )
        self.nom.pack(side="top")
        # Affichage du temps
        self.temps = Label(
            self,
            # ~text="Rien",
            height=1,
            width=9,
            # ~fg="dim grey",
            # ~fg='purple',
            # ~bg='ivory',
            # ~bg='grey',
            bg="grey70",
            font="Arial 25 bold"
            # ~font="Inconsolata 25 bold"
        )
        self.temps.pack(side="top")
        # Bouton marche/arrêt chronomètre
        self.bouton = Button(
            self,
            text="Start/Stop",
            # ~text='START',
            fg="black",
            # ~bg="coral",
            bg="Sandy Brown",
            # height=5,
            width=10,
            command=lambda: self.start_stop(),
        )
        self.bouton.pack(side="top", padx=5, pady=5)
        self.count()

    def count(self):
        if self.running == True:
            display = self.lancement_temps()
            self.temps["text"] = display  # Or temps.config(text=display)
            # ~self.temps['fg'] = 'black'
            # ~self.temps['fg'] = 'grey70'
            self.temps["fg"] = "white"
            self.temps.after(1000, self.count)
            self.counter += 1

    def lancement_temps(self):
        # ~tt = datetime.fromtimestamp(self.counter-3600) #workaround retirer une heure pour arriver à 0; pourquoi est-ce qu'il part à 01:00:00??
        tt = dt.datetime.fromtimestamp(self.counter - 3600)
        string = tt.strftime("%H:%M:%S")
        display = string
        return display

    def start_counter(self):
        self.running = True
        self.count()
        self.bouton["text"] = "STOP"

    def stop_counter(self):
        self.running = False
        self.bouton["text"] = "START"

    def start_stop(self):
        if self.running == False:
            self.start_counter()
        else:
            self.stop_counter()


class Tableau(Frame):
    """Fenêtre principale qui englobe les chronomètres"""

    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        # ~self.parent.title("Temps de parole")
        self.parent.title("Parlote équitable")

        self.chronos = {}
        for speaker in speakers:
            self.chronos[speaker] = Chrono(self, speaker, False)
        self.chronoglobal = ChronoGlobal(parent, qui="Discussion", running=True)

        # ~f = Frame()

        #  Bouton remise à zéro des conversations ## À voir si nécessaire plus tard / pas opérationnel de toute façon
        # ~self.remazero = Button(f, text="Remise\nà zéro", fg="purple", font="Inconsolata 15 bold", command=self.remise_a_zero) # Pas au point, comprens pas pourquoi
        # ~self.remazero.pack(anchor=S, padx=5, pady=5)
        # ~self.remazero.grid(row=2)

        # Discussion
        # ~self.chronoglobal.pack(f, padx=5, pady=5)

        # Bouton export
        self.export = Button(  # f,
            text="Exporter",
            fg="dark green",
            font="Inconsolata 15 bold",
            command=self.export,
        )
        # ~self.export.pack(anchor=N, padx=5, pady=5)

        # Bouton quitter
        self.terminer = Button(  # f,
            text="Quitter", fg="red", font="Inconsolata 15 bold", command=self.quit
        )
        # ~self.terminer.pack(anchor=S, padx=5, pady=5)
        # ~self.terminer.grid(row=2, column=3)

        # Trouver un moyen de les aligner automatiquement
        self.chronos["Selma"].grid(column=0, row=0, padx=5, pady=5)
        self.chronos["Alexandra"].grid(column=1, row=0, padx=5, pady=5)
        self.chronos["Francine"].grid(column=2, row=0, padx=5, pady=5)
        self.chronos["Charlotte"].grid(column=3, row=0, padx=5, pady=5)
        # ~self.chronoglobal.grid(column=3, row=0, padx=5, pady=5)
        self.chronos["Bernard"].grid(column=0, row=1, padx=5, pady=5)
        self.chronos["Miguel"].grid(column=1, row=1, padx=5, pady=5)
        self.chronos["Stéphane"].grid(column=2, row=1, padx=5, pady=5)

        self.chronoglobal.grid(column=3, row=1, padx=5, pady=5)

        self.export.grid(column=1, row=2, padx=5, pady=5)
        self.terminer.grid(column=2, row=2, padx=5, pady=5)
        # ~f.grid(column=3, row=1, padx=5, pady=5)
        # ~f.grid(column=2, row=3, padx=5, pady=5)
        # ~f.grid(column=0, row=2, padx=5, pady=5)
        # ~f.grid(column=1, row=2, padx=5, pady=5)
        # ~self.remazero.grid(column=3, row=1, padx=5, pady=5)
        # ~self.terminer.grid(column=4, row=1, padx=5, pady=5)

    def remise_a_zero(self):  # pas encore opérationnel
        for chrono in self.chronos:
            if self.chronos[chrono].running == True:
                # ce n'est pas ça qu'il faut, c'est réinitialiser le compteur, partir de tt fromtimestamp etc., en faire une méthode séparée réutilisable ici. Mais d'abord il faut arrêter tous les compteurs.
                self.chronos[chrono].running = False
            # ~display = self.chronos[chrono].lancement_temps()
            display = "00:00:00"  # Ne marche pas, ça reprend après où ça l'a laissé
            # ~self.chronos[chrono].temps.config(text=display)
            self.chronos[chrono].count()

    def export(self):
        # + ext_annee ## pas la peine de rajouter l'année si elle est déjà dans l'évènement
        self.nom_db = "CODA_Discussion_" + ext_jourheure
        db = shelve.open(self.nom_db)
        for s in speakers:
            db[s] = self.chronos[s].temps["text"]
        db[self.chronoglobal.qui] = self.chronoglobal.temps["text"]
        db.close()


# ~def main(args):
# ~return 0


if __name__ == "__main__":
    root = Tk()
    # ~root.geometry('800x600')
    tab = Tableau(parent=root)
    # ~tab.creer_chronos
    # ~print("Les chronos sont créés normalement")
    tab.mainloop()
    # ~root.mainloop()
    # ~import sys
    # ~sys.exit(main(sys.argv))
