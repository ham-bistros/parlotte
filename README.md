# parlotte

A project to count speech time of speakers in a conversation. Made to be run on a Raspberry Pi connected with buttons. Every time a speaker starts to talk they must push a physical button indicating they are speaking. It activates a personal timer, counting how much they have spoken during this conversation. If another speaker pushes their button, it stops the previous speaker's timer and activates their own.

The Raspberry Pi runs a Python [gunicorn]() server with a [websocket](), allowing a webpage to collect who is currently speaking and for how long, displaying in real-time if someone is monopolizing talking time.

Originally made for the development [Transquinquennal]() theater band for their and last show, with the secondary intention to being able to use it live, on stage.

Works on python 3.9.
