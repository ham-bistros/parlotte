#!/usr/bin/python

import socketio

import time
import csv
import datetime as dt

# From python-socketio Client API reference
# sio = socketio.Client(logger=True, engineio_logger=True)
sio = socketio.Client()
sio.connect("http://localhost:8000")


@sio.event
def connect():
    # print("I'm connected ! My sid is", sio.sid)
    pass


@sio.event
def connect_error(data):
    print("The connection failed!")


@sio.event
def disconnect():
    # print("I'm disconnected!")
    pass


@sio.event
def message(data):
    # print("I received a message!")
    pass


@sio.on("client_count")
def count(count):
    # print("There are %s connected clients." % count)
    pass


@sio.on("reset")
def reset():
    # print("---------- RESET ----------")
    global current_speaker
    current_speaker = ""

    global chronos
    chronos = {}

    global globalChrono
    globalChrono = Chrono("global")

    global logfile
    time.sleep(1)


@sio.on("speaker_name")
def spk_name(nom):
    # print("Maintenant c'est %s qui parle" % nom)
    pass


# LES DÉFINITIONS #


class Chrono:
    """Chronomètre, avec le nom de la personne, la durée, et le toggle"""

    def __init__(self, qui="Quelqu'un", running=False):
        self.qui = qui
        self.running = running
        self.debut_parole = 0
        self.fin_parole = 0
        self.total_parle = dt.timedelta(0)
        self.interventions = []
        self.stopped = False

    # Représentation en string de l'instance
    def __repr__(self) -> str:
        return f"{type(self).__name__}_{self.qui.lower()}"

    def start(self):
        if self.qui != "global":
            global current_speaker
            if current_speaker:
                chronos[current_speaker].pause()

            current_speaker = self.qui
            sio.emit("qui_parle", self.qui)

        self.running = True
        self.debut_parole = dt.datetime.now()

    def pause(self):
        if self.running:
            self.running = False
            self.fin_parole = dt.datetime.now()
            self.interventions.append(
                (self.debut_parole.timestamp(), self.fin_parole.timestamp())
            )
            self.total_parle += self.fin_parole - self.debut_parole
            sio.emit("stop_parle", current_speaker)
            with open("log.txt", "a", encoding="utf-8") as logfile:
                logfile.write("%s arrête de parler.\n" %
                              current_speaker.capitalize())

    def stop(self):
        self.stopped = True
        self.pause()

    def reset(self):
        if not self.stopped:
            self.stop()


def save_session():
    now = dt.datetime.now()

    # Fichier nommé à partir du moment de la fin de la session
    file = "discussions/%s.csv" % (now.strftime("%Y-%m-%d_%H:%M"))
    with open("log.txt", "a", encoding="utf-8") as logfile:
        logfile.write("Discussion enregistrée dans %s\n" % file)

    with open(file, "w", newline="") as csvfile:
        spamwriter = csv.writer(
            csvfile, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        global_interventions = [
            "|".join(str(t) for t in tuplou) for tuplou in globalChrono.interventions
        ]
        spamwriter.writerow(
            ["SESSION", ";".join(global_interventions),
             globalChrono.total_parle]
        )
        for name in chronos:
            name_interventions = [
                "|".join(str(t) for t in tuplou)
                for tuplou in chronos[name].interventions
            ]

            spamwriter.writerow(
                [
                    chronos[name].qui,
                    ";".join(name_interventions),
                    chronos[name].total_parle,
                ]
            )


speakers = [
    "alexandra",
    "bernard",
    "charlotte",
    "francine",
    "miguel",
    "selma",
    "stéphane",
]

first = True
globalChrono = Chrono("global")
chronos = {}
current_speaker = ""

if __name__ == "__main__":
    sio.emit("reset")

    while True:
        # time.sleep(1)
        sio.sleep(1)
        alors = input("nom: ")

        if alors == "pause" or alors == "p":
            print("→ pause")
            globalChrono.pause()
            sio.emit("pause_session")

            for name in chronos:
                chronos[name].pause()

        elif alors == "reset" or alors == "r":
            print("→ reset")
            globalChrono.reset()
            sio.emit("reset")
            current_speaker = ""

            for name in chronos:
                chronos[name].reset()
            save_session()

        else:
            try:
                nom = speakers[int(alors) % len(speakers)]

                if first:
                    globalChrono.start()
                    first = False

                if nom not in chronos:
                    chronos[nom] = Chrono(nom)

                if current_speaker:
                    chronos[current_speaker].pause()

                    if nom != current_speaker:
                        chronos[nom].start()
                        current_speaker = nom
                    else:
                        current_speaker = ""

                else:
                    chronos[nom].start()
                    current_speaker = nom

            except ValueError:
                # Faute de frappe
                pass
