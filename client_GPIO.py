import socketio
import RPi.GPIO as GPIO
import time
import csv
import datetime as dt
usr/bin/python


# From python-socketio Client API reference
# sio = socketio.Client(logger=True, engineio_logger=True)
sio = socketio.Client()
sio.connect("http://localhost:8000")


@sio.event
def connect():
    # print("I'm connected ! My sid is", sio.sid)
    pass


@sio.event
def connect_error(data):
    print("The connection failed!")


@sio.event
def disconnect():
    # print("I'm disconnected!")
    pass


@sio.event
def message(data):
    # print("I received a message!")
    pass


@sio.on("client_count")
def count(count):
    # print("There are %s connected clients." % count)
    pass


@sio.on("reset")
def reset():
    print("---------- RESET ----------")
    global current_speaker
    current_speaker = ""

    global chronos
    chronos = {}

    global globalChrono
    globalChrono = Chrono("global")

    global first
    first = True

    time.sleep(.2)


@sio.on("speaker_name")
def spk_name(nom):
    # print("Maintenant c'est %s qui parle" % nom)
    pass


# LES DÉFINITIONS #


class Chrono:
    """Chronomètre, avec le nom de la personne, la durée, et le toggle"""

    def __init__(self, qui="Quelqu'un", running=False):
        self.qui = qui
        self.running = running
        self.debut_parole = 0
        self.fin_parole = 0
        self.total_parle = dt.timedelta(0)
        self.interventions = []
        self.stopped = False

    # Représentation en string de l'instance
    def __repr__(self) -> str:
        return f"{type(self).__name__}_{self.qui.lower()}"

    def start(self):
        if self.qui != "global":
            global current_speaker
            if current_speaker:
                chronos[current_speaker].pause()

            current_speaker = self.qui
            sio.emit("qui_parle", self.qui)

        self.running = True
        self.debut_parole = dt.datetime.now()

    def pause(self):
        if self.running:
            self.running = False
            self.fin_parole = dt.datetime.now()
            self.interventions.append(
                (self.debut_parole.timestamp(), self.fin_parole.timestamp())
            )
            self.total_parle += self.fin_parole - self.debut_parole
            sio.emit("stop_parle", current_speaker)

    def stop(self):
        self.stopped = True
        self.pause()

    def reset(self):
        if not self.stopped:
            self.stop()


def save_session():
    print(globalChrono.total_parle.total_seconds())

    if globalChrono.interventions and globalChrono.total_parle.total_seconds() > 5:
        now = dt.datetime.now()

        # Fichier nommé à partir du moment de la fin de la session
        file = "discussions/%s.csv" % (now.strftime("%Y-%m-%d_%H:%M"))

        with open(file, "w", newline="") as csvfile:
            spamwriter = csv.writer(
                csvfile, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
            )
            global_interventions = [
                "|".join(str(t) for t in tuplou) for tuplou in globalChrono.interventions
            ]
            spamwriter.writerow(
                ["SESSION", ";".join(global_interventions),
                 globalChrono.total_parle]
            )
            for name in chronos:
                name_interventions = [
                    "|".join(str(t) for t in tuplou)
                    for tuplou in chronos[name].interventions
                ]

                spamwriter.writerow(
                    [
                        chronos[name].qui,
                        ";".join(name_interventions),
                        chronos[name].total_parle,
                    ]
                )


with open("speakers", "r", encoding="utf-8") as f:
    speakers = f.read().splitlines()

first = True
globalChrono = Chrono("global")
chronos = {}
current_speaker = ""

# Define pin mode : BCM vs physical (BCM works on all rpi gen)
GPIO.setmode(GPIO.BCM)

# Define GPIO pins - [0] is not used so that button 1 = input_pin[1]
input_pins = [21, 22, 23, 24, 25, 26, 27, 18]
button_pressed = [False] * len(input_pins)

# set GPIO pins as inputs with pull-up resitors
# pull-up means that input is always True except when pressed (sinked)
GPIO.setup(input_pins, GPIO.IN, pull_up_down=GPIO.PUD_UP)

sio.emit("reset")

# Loop
while True:

    # WTF ça marchait avant ???
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(input_pins, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    input_status = []  # reset of array at each cycle
    # Reads each input pin and stores in array
    for pin in input_pins:
        input_status.append(GPIO.input(pin))

    # input_status[i] will be 0 if button i is pressed
    for i in range(len(input_status)):
        nom = speakers[i]

        if input_status[i] == 0 and not button_pressed[i]:  # 0 = triggered
            print(f"Bouton {nom} appuyé")

            if nom == "RESET":
                # premier appui sur le bouton reset
                premier_appui = dt.datetime.now()

                print("→ pause")
                globalChrono.pause()
                sio.emit("pause_session")

            else:
                for name in chronos:
                    chronos[name].pause()
                if first:
                    globalChrono.start()
                    first = False

                if nom not in chronos:
                    chronos[nom] = Chrono(nom)

                if current_speaker:
                    chronos[current_speaker].pause()

                    if nom != current_speaker:
                        chronos[nom].start()
                        current_speaker = nom
                    else:
                        current_speaker = ""

                else:
                    chronos[nom].start()
                    current_speaker = nom

            # memory that it has been pressed so print is done only once
            button_pressed[i] = True

        elif nom == "RESET" and input_status[i] == 0 and not button_pressed[i]:
            # premier appui sur le bouton reset
            premier_appui = dt.datetime.now()

            print("→ pause")
            globalChrono.pause()
            sio.emit("pause_session")

            for name in chronos:
                chronos[name].pause()

            # memory that it has been pressed so print is done only once
            button_pressed[i] = True

        # 0 = triggered
        elif nom == "RESET" and input_status[i] == 0 and button_pressed[i]:
            # check du temps ou le bouton reste appuyé
            appuis_suivants = dt.datetime.now()
            diff = appuis_suivants - premier_appui
            if diff.total_seconds() > 5.0:
                print("→ reset")
                globalChrono.reset()
                sio.emit("reset")
                current_speaker = ""

            for name in chronos:
                chronos[name].reset()
#!/
            save_session()

        elif input_status[i] == 1:  # 0 = triggered
            # button off or released
            button_pressed[i] = False

    # add a short dealy to reduce CPU usage
    # if delay to high -> miss detection
    time.sleep(0.2)
#
# Clean up GPIO pins
    GPIO.cleanup()
