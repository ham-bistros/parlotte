#!/usr/bin/python
import socketio

# sio = socketio.Server()
sio = socketio.Server(debug=True, logger=True, engineio_logger=True)
app = socketio.WSGIApp(sio, static_files={"/": "./public/"})

client_count = 0


def start_speaking():
    print("coucou :)")
    speaker = input("parle: ")
    print(speaker + " is speaking")
    sio.emit("update", speaker)


@sio.event
def connect(sid, environ):
    # sid = session_id
    global client_count

    client_count += 1
    print(sid, "connected")
    sio.emit("client_count", client_count)

    # TODO: function qui envoie tout ce qui s'est passé aux clients
    # qui se reconnectent


@sio.event
def disconnect(sid):
    global client_count
    client_count -= 1
    print(sid, "disconnected")
    sio.emit("client_count", client_count)


@sio.on("pause_session")
def pause_session(sid):
    sio.emit("pause_session")


@sio.on("stop_session")
def stop_session(sid):
    sio.emit("stop_session")


@sio.on("reset")
def reset(sid):
    # TODO: reset function qui réinitialise tout ce qui s'est passé
    sio.emit("reset")


@sio.on("qui_parle")
def qui_parle(sid, data):
    print(data, "commence à parler.")
    sio.emit("speaker_name", data.lower())


@sio.on("stop_parle")
def stop_parle(sid, data):
    print(data, "arrête de parler.")
    sio.emit("stop_parle", data.lower())
